﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Winscript : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Player")
		{
			GameManager.instance.winscreen.SetActive(true); // enables gameobject
			GameManager.instance.level.SetActive(false); // disables gameobject
		}
	}

}

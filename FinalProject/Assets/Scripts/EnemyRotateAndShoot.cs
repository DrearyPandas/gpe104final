﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotateAndShoot : MonoBehaviour 
{

	private Transform shotpoint; // stores own transform
	private Rigidbody2D rb; // stores own rigidbody
	public GameObject playerLoc; // store player gameobject
	private float AngleRad; // stores angle in radians
	private float angle; // stores angle
	public GameObject bullet; // store bullet prefab
	private float shootTime; // stores current shot wait time
	public float shootWait; // stores shot wait time
	

	// Use this for initialization
	void Start () 
	{
		playerLoc = GameManager.instance.player; // sets variable to player object
		shotpoint = GetComponent<Transform>(); // sets own transform
		rb = GetComponent<Rigidbody2D>(); // sets own rigidbody
	}
	
	// Update is called once per frame
	void Update () 
	{
		shootTime -= Time.deltaTime; // decrements shoot timer over time
		AngleRad = Mathf.Atan2 (playerLoc.transform.position.y - shotpoint.position.y, playerLoc.transform.position.x - shotpoint.position.x); // gets angle of rotation from the enemy to the player
		angle = (180 / Mathf.PI) * AngleRad; // transforms angle to usable version
		
		rb.transform.localEulerAngles = new Vector3(0,0,angle); // rotates the object to face the player
	}
	
	public void Shoot()
	{
		if (shootTime <= 0)
		{
			Instantiate(bullet, shotpoint.position, shotpoint.rotation); // spawns bullet
			shootTime = shootWait; // resets timer
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillVol : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Player")
		{
			coll.gameObject.GetComponent<PlatformerMovement> ().Die(); // calls playerscript die function
		}
	}
}

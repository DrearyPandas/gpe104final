﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager instance; // gamemanager instance
	public float lives; // stores life count
	public GameObject player; // stores player gameobject
	public Vector3 spawnpoint; // stores player  spawn position
	public PlatformerMovement playerscript; // stores player script
	public List<GameObject> enemies; // stores list of enemies
	public Vector3 playerStart; // stores player start position
	public GameObject losescreen; // stores losescreen gameObject
	public GameObject winscreen; // stores winscreen gameObject
	public GameObject level; // stores level gameObject


	void Awake () 
	{
		if (instance == null) // if no instance
		{
			instance = this; // this is it
			DontDestroyOnLoad(gameObject); // dont destroy it
		}
		else// otherwise
		{
			Destroy(gameObject); // destroy this
		}
		instance = this; // this is the instance
		
		playerStart =  new Vector3(-11f, -4f, 0f); // initializes player start position
	}
	
	void Update()
	{
		if (lives <= 0)
		{
			level.SetActive(false); // disables gameobject
			losescreen.SetActive(true); // sets gameobject active
		}
	}
	
	public void Quit()
	{
		Application.Quit(); // quits game
	}
	
	public void ResetGame()
	{
		foreach(GameObject i in enemies)
		{
			i.gameObject.GetComponent<Enemy>().health = i.gameObject.GetComponent<Enemy>().maxhealth; // restores enemies to max health
			i.SetActive(true); // enables gameobject
		}
		
		player.transform.position = playerStart; // resets player position
		playerscript.checkPos = playerStart; // resets player checkpoint
		
		lives = 4; // resets player lives to max
	}
	
}

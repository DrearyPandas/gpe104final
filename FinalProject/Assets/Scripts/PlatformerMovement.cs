﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformerMovement : MonoBehaviour 
{

	//  references  //
	public Transform tf; // stores transform component
	public Rigidbody2D rb; // stores rigidbody component
	public Vector3 startPos; // stores player spawn location
	public SpriteRenderer sr; // stores spriterender component
	public Animator anim; // stores animator component
	public AudioSource audio; // stores audio source component
	public LayerMask Ground; // stores the layer to check for overlap
	public Transform groundCheck; // stores the position of the groundchecker gameobject
	public AudioClip jump; // stores jump sound
	public AudioClip die; // stores death sound
	public Vector3 checkPos; // stores checkpoint position



	// variables  //
	public bool grounded; // bool to store whether the player is on the ground
	public bool releasedJump; // bool to store if the player has released the jump key yet
	public float groundCheckRadius; // float to store the radius of the groundchecker physics overlap circle
	public bool jumping; // bool to store animation state
	public bool movingLeft; // bool to store animation state
	public bool movingRight; // bool to store animation state
	public bool idle; // bool to store animation state
	public float moveSpeed; // float to store the player move speed
	public float jumpStr; // float to store the jump strength
	public float jumpLength; // float to store the max jump time
	public float jumpCurrent; // float to store the current jump time
	public float healthMax; // stores player max health
	public float health; // stores player current health


	// Use this for initialization
	void Awake()
	{
		GameManager.instance.playerscript = this; // sets gamemanager variable to this object
		GameManager.instance.player = this.gameObject; // sets gamemanager variable to this object
	}
	void Start () 
	{
		
		rb = GetComponent<Rigidbody2D>(); // gets and sets rigidbody component
		tf = GetComponent<Transform>(); // gets and sets transform component
		sr = GetComponent<SpriteRenderer>(); // gets and sets Spriterenderer component
		anim = GetComponent<Animator>(); // gets and sets animator component
		audio = GetComponent<AudioSource>(); // gets and sets audio source component
		checkPos = tf.position; // sets startposition to current position
		jumpCurrent = jumpLength; // sets current jump time to max jump time
		
	}

	// Update is called once per frame
	void Update () 
	{
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, Ground); // checks if the groundchecker gamepbject is overlapped with any object in the ground layer and sets boolean

		if(grounded)
		{
			jumping = false; // sets jumping to false
			jumpCurrent = jumpLength; // sets current jump timer to max jump time
			rb.velocity = (rb.velocity / 3); // slows player by 1/3 every frame while touching ground
		}
		if(!grounded)
		{
			idle = false; // sets bool to false
			jumping = true; // sets bool to true
		}

		if (Input.GetKey(KeyCode.A))
		{
			rb.velocity = new Vector3 ((-1 * moveSpeed), rb.velocity.y, 0); // moves the player left at time times movespeed
			idle = false; // sets bool to false
			movingLeft = true; // sets bool to true
			movingRight = false; // sets bool to false
			sr.flipX = true; // flips the sprite to face left
		}

		if (Input.GetKey(KeyCode.D))
		{
			rb.velocity = new Vector3 ((1 * moveSpeed), rb.velocity.y, 0); // moves the player right at time times movespeed
			idle = false; // sets bool to false
			movingLeft = false; // sets bool to false
			movingRight = true; // sets bool to true
			sr.flipX = false; // flips the sprite to face right
		}

		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) 
		{
			if(grounded)
			{
				rb.velocity = new Vector3 (rb.velocity.x, jumpStr); // sets player velocity to jumpstrength in the positive x direction
				releasedJump = false; // sets bool to false
				idle = false; // sets bool to false
				jumping = true; // sets bool to true
				grounded = false; // sets bool to false
				audio.PlayOneShot(jump); // plays jump sound

			}
		}

		if(Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.Space))
		{
			jumpCurrent = 0; // sets current jump timer to zero
			releasedJump = true; // sets bool to true
		}

		if((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space)) && !releasedJump)
		{
			if(jumpCurrent > 0)
			{
				rb.velocity = new Vector3 (rb.velocity.x, jumpStr); // sets player velocity to jumpstrength in the positive x direction
				jumpCurrent -= Time.deltaTime; // decrements jumpcurrent timer
			}
		}

		if (!Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D) && !jumping)
		{
			movingLeft = false; // sets bool to false
			movingRight = false; // sets bool to false
			idle = true; // sets bool to true
		}

		if(movingLeft)
		{
			if(!jumping)
			{
				anim.Play("Run"); // plays the run animation
			}
		}
		if(movingRight)
		{
			if(!jumping)
			{
				anim.Play("Run"); // plays the run animation
			}
		}
		if(jumping)
		{
			anim.Play("Jump"); // plays the jump animation
		}
		if(idle)
		{
			anim.Play("idle"); // plays the idle animation
		}
		
		if (health <= 0)
		{
			Die(); // calls die function
		}
	}

	public void Die()
	{
		health = healthMax; // sets health to max health
		tf.position = checkPos; // resets player position to checkpoint
		audio.PlayOneShot(die); // plays death noise
		GameManager.instance.lives -= 1; // decrements lives
	}
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour 
{
	private Rigidbody2D rb; // stores rigidbody component
	public float speed; // stores speed variable
	private float timer = 0; // stores current lifetime
	public float destroytimer = 10; // store max lifetime
	
	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody2D>(); // gets and sets rigidbody
		rb.AddForce(transform.right * speed); // adds force scaled by speed
	}
	
	void Update()
	{
		timer += (1 * Time.deltaTime); // increments timer
		
		if (timer >= destroytimer)
		{
			Destroy(this.gameObject); // destroys object
		}
		
	}
	
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Enemy")
		{
			coll.gameObject.GetComponent<Enemy>().health -= 1; // reduces health variable by one
			Destroy(this.gameObject); // destroys self
			
		}
		
		if (coll.gameObject.tag != "Player" && coll.gameObject.tag != "Checkpoint")
		{
			Destroy(this.gameObject); // destroys self
		}
		
	}

}

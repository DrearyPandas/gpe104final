﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
	public GameObject ptf; // store player transform
	public float detectRad; // stores detection radius
	public float health; // stores current health
	public float maxhealth; // stores max health
	public bool detectPlayer; // sotres true/false for player detection
	private float dist; // stores distance from player
	public GameObject shotpoint; // stores shotpoint gameObject
	
	// Use this for initialization
	void Start () 
	{
		ptf = GameManager.instance.player; // gets and sets player transform
		GameManager.instance.enemies.Add(this.gameObject); // adds this gameObject to the list
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		dist = Vector3.Distance(ptf.transform.position, transform.position); // gets distance from enemy to player
		if (dist <= detectRad)
		{
			Vector3 localPosition = ptf.transform.position - transform.position; // take difference of player location and self location
			localPosition = localPosition.normalized; // normalize vector
			RaycastHit2D hit = Physics2D.Raycast(transform.position, localPosition, 999);// raycast from enemy origin in direction of player with very large(infinite) range
			if(hit.collider.gameObject.tag == "Player")
			{
				detectPlayer = true; // sets bool to true
			}
			else
			{
				detectPlayer = false; // sets bool to false
			}
		}
		else
		{
			detectPlayer = false; // sets bool to false
		}

		
		if(detectPlayer)
		{
			shotpoint.GetComponent<EnemyRotateAndShoot>().Shoot(); // gets and calls the shoot function from the shoot script
		}
		
		if (health <= 0)
		{
			gameObject.SetActive(false); // disables this object
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAndShoot : MonoBehaviour 
{
	private Camera cam; // stores camera 
	private Transform shotpoint; //  stores shotpoint transform
	private Rigidbody2D rb; // stores rigidbody component
	private Vector3 mouseloc; // stores mouse location
	private float AngleRad; // stores angle in radians
	private float angle; // store angle in degree
	public GameObject bullet; // stores bullet prefab

	// Use this for initialization
	void Start () 
	{
		cam = Camera.main; // sets camera to main camera
		shotpoint = GetComponent<Transform>(); // gets and sets transform
		rb = GetComponent<Rigidbody2D>(); //gets and sets rigidbody component
	}
	
	// Update is called once per frame
	void Update () 
	{
		mouseloc = cam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0)); // gets mouse position from camera
		
		AngleRad = Mathf.Atan2 (mouseloc.y - shotpoint.position.y, mouseloc.x - shotpoint.position.x); // gets radian measurement from transform to mouse location
		angle = (180 / Mathf.PI) * AngleRad; // converts to degree
		
		rb.transform.localEulerAngles = new Vector3(0,0,angle); // rotates object
		
		
		if (Input.GetMouseButtonDown(0))
		{
			Instantiate(bullet, shotpoint.position, shotpoint.rotation); // creates bullet
		}
	}
}

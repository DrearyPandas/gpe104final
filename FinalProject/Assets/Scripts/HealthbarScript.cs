﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthbarScript : MonoBehaviour 
{
	public Image healthbar; // stores hp bar image

	
	// Update is called once per frame
	void Update () 
	{
		healthbar.fillAmount = GameManager.instance.playerscript.health / GameManager.instance.playerscript.healthMax; // gets a percentile value for current hp related to max hp
	}
}
